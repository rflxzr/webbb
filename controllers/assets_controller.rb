class AssetsController < BaseController
  namespace '/assets' do
    get '/style/:filename' do
      required_params :filename
      scss (("style/#{params[:filename]}").to_sym)
    end
  end
end
