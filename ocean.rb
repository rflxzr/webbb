require 'rubygems'
require 'bundler'
Bundler.require

require_rel 'models'
require_rel 'helpers'

class Ocean < Sinatra::Base
  set :bind, "0.0.0.0"
  enable :sessions
  set :root, File.dirname(__FILE__)
  disable :raise_errors if ENV["NOW"]
  disable :show_exceptions if ENV["NOW"]
  set :haml, format: :html5

  register Sinatra::Namespace
  helpers Authorization
  helpers Sinatra::RequiredParams

  configure do
    MongoMapper.setup( {'production' => { 'uri' => ENV['DATABASE_URI'] } }, 'production')
    set :search, SwiftypeAppSearch::Client.new(:account_host_key => ENV["SEARCH_HOST_KEY"], :api_key => ENV["SEARCH_API_KEY"])
  end

  get '/' do
    haml :index
  end
end

require_relative './controllers/base_controller.rb'
require_rel 'controllers'

Ocean.run! if __FILE__ == $0