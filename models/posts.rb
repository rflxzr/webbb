class Post
	include MongoMapper::Document
	
	key :title, String
	key :slug, String
	key :author, String
	key :content, String
	key :time, String
	timestamps!
end