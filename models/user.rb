class User
	include MongoMapper::Document
	
	key :username, String
	key :first_name, String
	key :last_name, String
	key :digest, String
	key :admin, Boolean
	timestamps!
end