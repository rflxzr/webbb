module Authorization
  def authorized?
    not session[:userID].nil?
  end

  def login! user, password
  end

  def logout!
  end

  def protected! admin = false
  end
end